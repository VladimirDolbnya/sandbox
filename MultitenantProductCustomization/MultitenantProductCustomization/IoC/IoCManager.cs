﻿using System;
using Microsoft.Practices.Unity;

namespace MultiTenantProductCustomization.IoC
{
	public static class IoCManager
	{
		public static UnityContainer Container { get; }

		static IoCManager()
		{
			Container = new UnityContainer();
		}
	}
}
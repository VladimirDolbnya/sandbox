﻿using System.Web;
using MultiTenantProductCustomization.Infastructure.TenantsManagement;

namespace MultiTenantProductCustomization
{
	public class HttpContextTenantManager : ITenantManager
	{
		private const string TenantIdKey = "TenantId";

		private const string TenantNameKey = "TenantName";

		public void SetCurrentTenant(int tenantId, string tenantName)
		{
			HttpContext.Current.Session[TenantIdKey] = tenantId;
			HttpContext.Current.Session[TenantNameKey] = tenantName;
		}

		public void ClearCurrentTenant()
		{
			HttpContext.Current.Session[TenantIdKey] = null;
			HttpContext.Current.Session[TenantNameKey] = null;
		}

		public int? GetCurrentTenantId()
		{
			return HttpContext.Current.Session[TenantIdKey]  as int?;
		}

		public string GetCurrentTenantName()
		{
			return HttpContext.Current.Session[TenantNameKey] as string;
		}
	}
}
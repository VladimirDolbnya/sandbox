﻿using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;
using MultiTenantProductCustomization.IoC;
using Microsoft.Practices.Unity;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement
{
    internal class TenantCommandInterceptor : IDbCommandInterceptor
    {
        public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            SetTenantParameterValue(command);
        }

        public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
        }

        public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            SetTenantParameterValue(command);
        }

        public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
        }

        public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            SetTenantParameterValue(command);
        }

        public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
        }

        private static void SetTenantParameterValue(DbCommand command)
        {
			var currentTenantId =  IoCManager.Container.Resolve<ITenantManager>().GetCurrentTenantId();
			if (command == null || command.Parameters.Count == 0 || currentTenantId == null)
                return;

			var parametersToUpdate = command
				.Parameters
				.Cast<DbParameter>()
				.Where(param => param.ParameterName == TenantAwareAttribute.TenantIdFilterParameterName)
				.ToList();

			parametersToUpdate.ForEach(x=> x.Value = currentTenantId.Value);
        }
    }
}
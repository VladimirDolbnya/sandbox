﻿using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement
{
    public class TenantQueryVisitor : DefaultExpressionVisitor
    {
        private bool _injectedDynamicFilter;

        public override DbExpression Visit(DbFilterExpression expression)
        {
            var column = TenantAwareAttribute.GetTenantColumnName(expression.Input.Variable.ResultType.EdmType);
	        if (_injectedDynamicFilter || string.IsNullOrEmpty(column))
				return base.Visit(expression);

	        var newFilterExpression = BuildFilterExpression(expression.Input, expression.Predicate, column);
	        return base.Visit(newFilterExpression ?? expression);
        }

        public override DbExpression Visit(DbScanExpression expression)
        {
            var column = TenantAwareAttribute.GetTenantColumnName(expression.Target.ElementType);
	        if (_injectedDynamicFilter || string.IsNullOrEmpty(column))
				return base.Visit(expression);

	        var dbExpression = base.Visit(expression);
	        var currentExpressionBinding = DbExpressionBuilder.Bind(dbExpression);
	        var newFilterExpression = BuildFilterExpression(currentExpressionBinding, null, column);
	        return newFilterExpression != null ? base.Visit(newFilterExpression) : base.Visit(expression);
        }

        private DbFilterExpression BuildFilterExpression(DbExpressionBinding binding, DbExpression predicate, string column)
        {
            _injectedDynamicFilter = true;
            var variableReference = binding.VariableType.Variable(binding.VariableName);
            var tenantProperty = variableReference.Property(column);
            var tenantParameter = tenantProperty
				.Property
				.TypeUsage.Parameter(TenantAwareAttribute.TenantIdFilterParameterName);

			DbExpression newPredicate = tenantProperty.Equal(tenantParameter);

            if (predicate != null)
                newPredicate = newPredicate.And(predicate);

            return binding.Filter(newPredicate);
        }

    }
}
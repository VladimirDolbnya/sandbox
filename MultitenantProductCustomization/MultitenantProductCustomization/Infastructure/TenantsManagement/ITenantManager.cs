﻿namespace MultiTenantProductCustomization.Infastructure.TenantsManagement
{
	public interface ITenantManager
	{
		void SetCurrentTenant(int tenantId, string tenantName);

		void ClearCurrentTenant();

		int? GetCurrentTenantId();

		string GetCurrentTenantName();
	}
}

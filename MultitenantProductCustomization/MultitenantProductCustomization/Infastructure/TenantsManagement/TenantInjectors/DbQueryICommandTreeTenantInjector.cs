﻿using System;
using System.Data.Entity.Core.Common.CommandTrees;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement.TenantInjectors
{
	public class DbQueryICommandTreeTenantInjector : ICommandTreeTenantInjector
	{
		private readonly DbQueryCommandTree _commandTree;

		public DbQueryICommandTreeTenantInjector(DbQueryCommandTree commandTree)
		{
			if (commandTree == null)
				throw new ArgumentNullException(nameof(commandTree));

			_commandTree = commandTree;
		}

		public bool TryModifyCommandTree(out DbCommandTree modifiedCommandTree)
		{
			var newQuery = _commandTree.Query.Accept(new TenantQueryVisitor());
			modifiedCommandTree = new DbQueryCommandTree(
				_commandTree.MetadataWorkspace,
				_commandTree.DataSpace,
				newQuery);

			return true;
		}
	}
}
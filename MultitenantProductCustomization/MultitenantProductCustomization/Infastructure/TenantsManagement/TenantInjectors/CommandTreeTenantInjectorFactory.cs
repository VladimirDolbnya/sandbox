﻿using System.Data.Entity.Core.Common.CommandTrees;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement.TenantInjectors
{
	public static class CommandTreeTenantInjectorFactory
	{
		public static ICommandTreeTenantInjector Create(DbCommandTree commandTree)
		{
			var queryCommandTree = commandTree as DbQueryCommandTree;
			if(queryCommandTree!=null)
				return new DbQueryICommandTreeTenantInjector(queryCommandTree);

			var insertCommandTree = commandTree as DbInsertCommandTree;
			if(insertCommandTree!=null) 
				return new DbInsertCommandTreeTenantInjector(insertCommandTree);

			var updateCommandTree = commandTree as DbUpdateCommandTree;
			if (updateCommandTree != null)
				return new DbUpdateCommandTreeTenantInjector(updateCommandTree);

			var deleteCommandTree = commandTree as DbDeleteCommandTree;
			if (deleteCommandTree != null)
				return new DbDeleteCommandTreeTenantInjector(deleteCommandTree);

			return null;

		}
	}
}
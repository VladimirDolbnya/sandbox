﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement.TenantInjectors
{
	public class DbUpdateCommandTreeTenantInjector : DbModificationCommandTreeTenantInjector
	{
		private readonly DbUpdateCommandTree _commandTree;

		public DbUpdateCommandTreeTenantInjector(DbUpdateCommandTree commandTree) : base(commandTree)
		{
			_commandTree = commandTree;
		}

		protected override DbCommandTree ModifyCommandTree(
			string column, 
			int? tenantId, 
			DbVariableReferenceExpression variableReference,
			DbPropertyExpression tenantProperty)
		{
			var tenantIdWherePredicate = tenantProperty.Equal(DbExpression.FromInt32(tenantId));

			var filteredSetClauses =_commandTree
				.SetClauses
				.Cast<DbSetClause>()
				.Where(sc => ((DbPropertyExpression)sc.Property).Property.Name != column);

			var finalSetClauses = new ReadOnlyCollection<DbModificationClause>(new List<DbModificationClause>(filteredSetClauses));

			var initialPredicate = _commandTree.Predicate;
			var finalPredicate = initialPredicate.And(tenantIdWherePredicate);

			return  new DbUpdateCommandTree(
				_commandTree.MetadataWorkspace,
				_commandTree.DataSpace,
				_commandTree.Target,
				finalPredicate,
				finalSetClauses,
				_commandTree.Returning);
		}
	}
}
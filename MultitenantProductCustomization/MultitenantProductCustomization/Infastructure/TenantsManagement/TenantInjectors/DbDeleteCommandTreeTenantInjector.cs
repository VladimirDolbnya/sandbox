﻿using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement.TenantInjectors
{
	public class DbDeleteCommandTreeTenantInjector : DbModificationCommandTreeTenantInjector
	{
		private readonly DbDeleteCommandTree _commandTree;

		public DbDeleteCommandTreeTenantInjector(DbDeleteCommandTree commandTree) : base(commandTree)
		{
			_commandTree = commandTree;
		}

		protected override DbCommandTree ModifyCommandTree(
			string column, 
			int? tenantId, 
			DbVariableReferenceExpression variableReference,
			DbPropertyExpression tenantProperty)
		{
			var tenantIdWherePredicate = DbExpressionBuilder.Equal(tenantProperty, DbExpression.FromInt32(tenantId));
			var initialPredicate = _commandTree.Predicate;
			var finalPredicate = initialPredicate.And(tenantIdWherePredicate);
			return new DbDeleteCommandTree(
				_commandTree.MetadataWorkspace,
				_commandTree.DataSpace,
				_commandTree.Target,
				finalPredicate);
		}
	}
}
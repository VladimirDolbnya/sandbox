﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement.TenantInjectors
{
	public class DbInsertCommandTreeTenantInjector : DbModificationCommandTreeTenantInjector
	{
		private readonly DbInsertCommandTree _commandTree;

		public DbInsertCommandTreeTenantInjector(DbInsertCommandTree commandTree) : base(commandTree)
		{
			_commandTree = commandTree;
		}

		protected override DbCommandTree ModifyCommandTree(
			string column,
			int? tenantId,
			DbVariableReferenceExpression variableReference, 
			DbPropertyExpression tenantProperty)
		{
			var tenantSetClause = DbExpressionBuilder.SetClause(tenantProperty, DbExpression.FromInt32(tenantId));
			var filteredSetClauses =
				_commandTree.SetClauses.Cast<DbSetClause>()
					.Where(sc => ((DbPropertyExpression)sc.Property).Property.Name != column);

			var finalSetClauses = new ReadOnlyCollection<DbModificationClause>(new List<DbModificationClause>(filteredSetClauses)
				{
					tenantSetClause
				});

			return new DbInsertCommandTree(
				_commandTree.MetadataWorkspace,
				_commandTree.DataSpace,
				_commandTree.Target,
				finalSetClauses,
				_commandTree.Returning);
		}
	}
}
﻿using System;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using MultiTenantProductCustomization.IoC;
using Microsoft.Practices.Unity;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement.TenantInjectors
{
	public abstract class DbModificationCommandTreeTenantInjector : ICommandTreeTenantInjector
	{
		private readonly DbModificationCommandTree _commandTree;

		protected  DbModificationCommandTreeTenantInjector(DbModificationCommandTree commandTree)
		{
			if (commandTree == null)
				throw new ArgumentNullException(nameof(commandTree));

			_commandTree = commandTree;
		}

		public bool TryModifyCommandTree(out DbCommandTree modifiedCommandTree)
		{
			modifiedCommandTree = null;
			var column = TenantAwareAttribute.GetTenantColumnName(_commandTree.Target.VariableType.EdmType);
			if (string.IsNullOrEmpty(column))
				return false;

			var tenantId = IoCManager.Container.Resolve<ITenantManager>().GetCurrentTenantId();
			if (!tenantId.HasValue)
				throw new InvalidOperationException($"Attempt to execute modification command on tenant-aware entity where tenant is not specified");

			var variableReference = _commandTree.Target.VariableType.Variable(_commandTree.Target.VariableName);
			var tenantProperty = variableReference.Property(column);

			modifiedCommandTree = ModifyCommandTree(column, tenantId, variableReference, tenantProperty);
			return true;
		}

		protected abstract DbCommandTree ModifyCommandTree(
			string column, 
			int? tenantId,
			DbVariableReferenceExpression variableReference, 
			DbPropertyExpression tenantProperty);
	}
}
﻿using System.Data.Entity.Core.Common.CommandTrees;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement.TenantInjectors
{
	public interface ICommandTreeTenantInjector
	{
		bool TryModifyCommandTree(out DbCommandTree modifiedCommandTree);
	}
}
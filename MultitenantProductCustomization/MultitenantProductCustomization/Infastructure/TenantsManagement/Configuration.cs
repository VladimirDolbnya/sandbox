﻿using System.Data.Entity;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement
{
    public class EntityFrameworkConfiguration : DbConfiguration
    {
        public EntityFrameworkConfiguration()
        {
            AddInterceptor(new TenantCommandInterceptor());
            AddInterceptor(new TenantCommandTreeInterceptor());
        }
    }
}
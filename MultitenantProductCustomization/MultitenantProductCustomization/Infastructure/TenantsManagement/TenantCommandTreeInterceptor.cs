﻿using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure.Interception;
using MultiTenantProductCustomization.Infastructure.TenantsManagement.TenantInjectors;

namespace MultiTenantProductCustomization.Infastructure.TenantsManagement
{
    public class TenantCommandTreeInterceptor : IDbCommandTreeInterceptor
    {
        public void TreeCreated(DbCommandTreeInterceptionContext interceptionContext)
        {
	        if (interceptionContext.OriginalResult.DataSpace != DataSpace.SSpace)
				return;

	        var injector = CommandTreeTenantInjectorFactory.Create(interceptionContext.OriginalResult);
			DbCommandTree updatedCommandTree;
	        if (injector != null && injector.TryModifyCommandTree(out updatedCommandTree))
		        interceptionContext.Result = updatedCommandTree;
        }
    }
}
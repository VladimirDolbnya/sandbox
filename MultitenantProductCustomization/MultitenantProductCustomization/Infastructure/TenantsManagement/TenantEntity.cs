﻿
namespace MultiTenantProductCustomization.Infastructure.TenantsManagement
{
	[TenantAware("TenantId")]
	public class TenantEntity
	{
		public int TenantId { get; private set; }

		public Tenant Tenant { get; set; }
	}
}
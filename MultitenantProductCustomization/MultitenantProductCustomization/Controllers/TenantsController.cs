﻿using System.Linq;
using System.Web.Mvc;
using MultiTenantProductCustomization.Controllers.ProductManagement;
using MultiTenantProductCustomization.Infastructure.TenantsManagement;
using MultiTenantProductCustomization.IoC;
using MultiTenantProductCustomization.Models.Shared;
using Microsoft.Practices.Unity;

namespace MultiTenantProductCustomization.Controllers
{
	public class TenantsController :ProductControllerBase
	{
		public ActionResult Index()
		{
			var tenants = Context
				.Tenants
				.Select(x => new ItemWitnName {Id = x.Id, DisplayName = x.Name})
				.ToList();

			return View(tenants);
		}

		public ActionResult Create()
		{
			return View(new ItemWitnName());
		}

		[HttpPost]
		public ActionResult Create(ItemWitnName model)
		{
			Context.Tenants.Add(new Tenant {Name = model.DisplayName});
			Context.SaveChanges();
			return RedirectToAction("Index");
		}


		public ActionResult UseTenant(int id)
		{
			var tenant = Context.Tenants.Find(id);
			IoCManager.Container.Resolve<ITenantManager>().SetCurrentTenant(tenant.Id, tenant.Name);
			return RedirectToAction("Index");
		}

		public ActionResult LeaveTenant()
		{
			IoCManager.Container.Resolve<ITenantManager>().ClearCurrentTenant();
			return RedirectToAction("Index");
		}
	}
}
﻿using System.Linq;
using System.Web.Mvc;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.Models.Products;
using MultiTenantProductCustomization.Models.Shared;

namespace MultiTenantProductCustomization.Controllers.ProductManagement
{
	public class ProductAttributesController : ProductControllerBase
	{
		public ActionResult Index()
		{
			var model = Context.ProductAttributes.Select(x => new ItemWitnName {Id = x.Id, DisplayName = x.Name}).ToList();
			return View(model);
		}

		public ActionResult View(int id)
		{
			var category = Context.GetAtrAttribute(id);
			var model = new ProductAttributeModel(category);
			return View(model);
		}

		[HttpPost]
		public ActionResult Create(AttributeModel model)
		{
			var productAttribute = new ProductAttribute {Name = model.Name};
			Context.ProductAttributes.Add(productAttribute);
			Context.SaveChanges();
			return RedirectToAction("View", new {id = productAttribute.Id});
		}

		public ActionResult Create()
		{
			return View(new AttributeModel());
		}
	}
}
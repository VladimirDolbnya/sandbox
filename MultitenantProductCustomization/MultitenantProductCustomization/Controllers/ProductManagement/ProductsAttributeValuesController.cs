﻿using System.Web.Mvc;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.Models.Products;

namespace MultiTenantProductCustomization.Controllers.ProductManagement
{
	public class ProductsAttributeValuesController : ProductControllerBase
	{
		public ActionResult Create(int categoryid)
		{
			var attribute = Context.GetAtrAttribute(categoryid);
			var model = new AttributeValueModel {AttributeId = attribute.Id, AttributeName = attribute.Name};
			return View(model);
		}

		[HttpPost]
		public ActionResult Create(AttributeValueModel model)
		{
			var attribute = Context.GetAtrAttribute(model.AttributeId);
			var result = new ProductAttributeValue {Attribute = attribute, Name = model.Name};
			Context.ProductAttributeValues.Add(result);
			Context.SaveChanges();
			return RedirectToAction("View", new {Controller = "ProductAttributes",id = attribute.Id});
		}
	}
}
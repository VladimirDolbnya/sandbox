﻿using System.Web.Mvc;
using MultiTenantProductCustomization.DomainModel;

namespace MultiTenantProductCustomization.Controllers.ProductManagement
{
	public class ProductControllerBase : Controller
	{
		protected readonly ProductsContext Context = new ProductsContext();

		protected override void Dispose(bool disposing)
		{
			if (disposing)
				Context.Dispose();

			base.Dispose(disposing);
		}
	}
}
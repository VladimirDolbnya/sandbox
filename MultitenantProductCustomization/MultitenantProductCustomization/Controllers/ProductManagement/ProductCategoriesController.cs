﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.Models.Products;
using MultiTenantProductCustomization.Models.Shared;

namespace MultiTenantProductCustomization.Controllers.ProductManagement
{
    public class ProductCategoriesController : ProductControllerBase
	{
		public ActionResult Index()
		{
			var productCategories = Context
				.ProductCategories
				.Select(x => new ItemWitnName {DisplayName = x.Name, Id = x.Id}).ToList();
			return View(productCategories);
		}

		[HttpPost]
		public ActionResult Edit(ProductCategoryModel model)
		{
			UpdateProductCategory(model);
			return RedirectToAction("Index");
		}

	    private void UpdateProductCategory(ProductCategoryModel model)
	    {
		    ProductCategory result;
		    if (model.Id>0)
		    {
			    result = Context.ProductCategories.Find(model.Id);
			}
			else
		    {
				result = new ProductCategory();
		    }

			Context.Entry(result).State = result.Id==0?EntityState.Added : EntityState.Modified;
		    result.Name = model.Name;
			var attributeCategoryIds = model.ProductCategories.Where(x=>x.Value).Select(x => x.Id).ToList();
			var categories = Context.ProductAttributes.Where(x => attributeCategoryIds.Contains(x.Id)).ToList();
			result.Attributes.Clear();
			categories.ForEach(result.Attributes.Add);
			Context.SaveChanges();
		}

	    public ActionResult Edit(int? id)
	    {
		    var productCategory = id == null ? new ProductCategory() : Context.ProductCategories.Find(id);
			if(productCategory==null)
				return HttpNotFound();

			var model = new ProductCategoryModel(Context, productCategory);
			return View( model);
	    }
    }
}

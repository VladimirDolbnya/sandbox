﻿using System.Linq;
using System.Web.Mvc;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.Models.Products;
using MultiTenantProductCustomization.Models.Shared;

namespace MultiTenantProductCustomization.Controllers.ProductManagement
{
    public class ProductsController : ProductControllerBase
	{
		// GET: Products
		public ActionResult Index(int? categoryId)
		{
			var category = Context.GetCategory(categoryId.Value);
			var model = new ProductsListModel
			{
				CategoryId = category.Id,
				CategoryName = category.Name,
				Products =
					Context.Products.Where(x => x.ProductCategory.Id == categoryId)
						.Select(x => new ItemWitnName {DisplayName = x.Name, Id = x.Id})
						.ToList()
			};
            return View(model);
        }


		[HttpPost]
		public ActionResult Create(ProductEditModel model)
		{
			var category = Context.ProductCategories.Find(model.ProductCategoryId);
			var result = new Product
			{
				Name = model.Name,
				ProductCategory = category
			};
			var attributeValueIds = model
				.Attributes
				.Where(x => x?.Value != null && x.Value > 0)
				.Select(x => x.Value)
				.Distinct()
				.ToList();

			var attributeValues = Context
				.ProductAttributeValues
				.Where(x => attributeValueIds.Contains(x.Id))
				.ToList();

			attributeValues.ForEach(result.Attributes.Add);
			Context.Products.Add(result);
			Context.SaveChanges();
			return RedirectToAction("Index",new {categoryId = category.Id});
		}

		public ActionResult Create(int categoryid)
	    {
		    var category = Context.ProductCategories.Find(categoryid);
			var model = new ProductEditModel(category);
		    return View("Edit", model);
	    }

	    public ActionResult View(int id)
	    {
		    var product = Context.Products.Find(id);
			var model = new ProductViewModel(product);
		    return View(model);
	    }

	    public ActionResult Filter(int categoryId)
	    {
		    var category = Context.GetCategory(categoryId);
		    var model = new ProductFilterModel(category);
		    return View(model);
	    }

		[HttpPost]
		public ActionResult Filter(ProductFilterModel model)
		{
			var categoryId = model.ProductCategoryId;
			var filter = model.Attributes.Where(x => x.Options.Any(y => y.Value))
				.ToDictionary(x => x.Id, x => x.Options.Where(y => y.Value).Select(y => y.Id).ToList());

			var products = Context.Products.Where(x => x.ProductCategory.Id == categoryId);
			foreach (var filterPart in filter)
				products = products.Where(x => x.Attributes.Any(y => y.Attribute.Id == filterPart.Key && filterPart.Value.Contains(y.Id)));

			var category = Context.GetCategory(categoryId);
			var result = new ProductsListModel
			{
				CategoryId = category.Id,
				CategoryName = category.Name,
				Products = products.Select(x => new ItemWitnName { DisplayName = x.Name, Id = x.Id }).ToList()
			};

			return View("FilterResult", result);
		}
	}

}

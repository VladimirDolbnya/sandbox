﻿namespace MultiTenantProductCustomization.Models.Shared
{
	public class CheckBoxListItem
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }

		public bool Value { get; set; }
	}
}
﻿
namespace MultiTenantProductCustomization.Models.Shared
{
	public class ItemWitnName
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }
	}
}
﻿using System.Collections.Generic;

namespace MultiTenantProductCustomization.Models.Shared
{
	public class RadioGroupListItem
	{
		public string DisplayName { get; set; }

		public List<ItemWitnName> Options { get; set; }
		
		public int? Value { get; set; } 
	}
}
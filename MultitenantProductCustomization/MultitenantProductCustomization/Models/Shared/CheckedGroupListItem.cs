﻿using System.Collections.Generic;

namespace MultiTenantProductCustomization.Models.Shared
{
	public class CheckedGroupListItem
	{
		public int Id { get; set; }

		public string DisplayName { get; set; }

		public List<CheckBoxListItem> Options { get; set; }

	}
}
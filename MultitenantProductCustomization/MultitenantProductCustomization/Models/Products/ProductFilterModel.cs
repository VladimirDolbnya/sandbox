﻿using System.Collections.Generic;
using System.Linq;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.Models.Shared;

namespace MultiTenantProductCustomization.Models.Products
{
	public class ProductFilterModel
	{
		public ProductFilterModel()
		{
			Attributes = new List<CheckedGroupListItem>();
		}

		public ProductFilterModel(ProductCategory category)
		{
			ProductCategoryId = category.Id;
			ProductCategoryName = category.Name;
			Attributes = category.Attributes.Select(CreateCheckedGroupListItem).ToList();
		}

		private CheckedGroupListItem CreateCheckedGroupListItem(ProductAttribute productAttribute)
		{
			var result = new CheckedGroupListItem
			{
				DisplayName = productAttribute.Name,
				Id = productAttribute.Id,
				Options = productAttribute
					.AttributeValues
					.Select(x => new CheckBoxListItem {Id = x.Id, DisplayName = x.Name})
					.ToList()
			};

			return result;
		}

		public int ProductCategoryId { get; set; }

		public string ProductCategoryName { get; set; }

		public List<CheckedGroupListItem> Attributes { get; set; } 
	}
}
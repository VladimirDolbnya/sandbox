﻿using System.Collections.Generic;
using System.Linq;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.Models.Shared;

namespace MultiTenantProductCustomization.Models.Products
{
	public class ProductAttributeModel
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public List<ItemWitnName> Attributes { get; set; }

		public ProductAttributeModel()
		{
			Attributes = new List<ItemWitnName>();
		}

		public ProductAttributeModel(ProductAttribute  category)
		{
			Id = category.Id;
			Name = category.Name;
			Attributes = category.AttributeValues.Select(x => new ItemWitnName {DisplayName = x.Name, Id = x.Id}).ToList();
		}
	}
}
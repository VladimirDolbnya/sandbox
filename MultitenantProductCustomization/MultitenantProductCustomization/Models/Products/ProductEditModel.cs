﻿using System.Collections.Generic;
using System.Linq;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.Models.Shared;

namespace MultiTenantProductCustomization.Models.Products
{
	public class ProductEditModel
	{
		public ProductEditModel()
		{
			Attributes = new List<RadioGroupListItem>();
		}

		public ProductEditModel(ProductCategory category)
			:this()
		{
			ProductCategoryName = category.Name;
			ProductCategoryId = category.Id;
			Attributes = category.Attributes.Select(CreateRadioButtonListItem).ToList();
		}

		private RadioGroupListItem CreateRadioButtonListItem(ProductAttribute productAttribute)
		{
			var result = new RadioGroupListItem
			{
				DisplayName= productAttribute.Name,
				Options = productAttribute.AttributeValues.Select(x=>new ItemWitnName {Id = x.Id,DisplayName =x.Name}).ToList()
			};

			return result;
		}

		public int Id { get; set; }

		public string Name { get; set; }

		public string ProductCategoryName { get; set; }

		public int ProductCategoryId { get; set; }

		public List<RadioGroupListItem> Attributes { get; set; } 
	}
}
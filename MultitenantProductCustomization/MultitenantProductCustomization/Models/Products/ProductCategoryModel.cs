﻿using System.Collections.Generic;
using System.Linq;
using MultiTenantProductCustomization.DomainModel;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.Models.Shared;
using WebGrease.Css.Extensions;

namespace MultiTenantProductCustomization.Models.Products
{
	public class ProductCategoryModel
	{
		public ProductCategoryModel()
		{
			ProductCategories  = new List<CheckBoxListItem>();
		}

		public ProductCategoryModel(ProductsContext context, ProductCategory productCategory = null)
		{
			ProductCategories = context
				.ProductAttributes
				.Select(x => new CheckBoxListItem { Id = x.Id, DisplayName = x.Name })
				.ToList();

			if (productCategory != null)
			{
				Id = productCategory.Id;
				Name = productCategory.Name;
				var selectedAttributeCategories = productCategory.Attributes.Select(x => x.Id);
				ProductCategories.Where(x=> selectedAttributeCategories.Contains(x.Id)).ForEach(x=>x.Value=true);
			}
		}

		public int Id { get; set; }

		public string Name { get; set; }

		public List<CheckBoxListItem> ProductCategories { get; set; } 
	}
}
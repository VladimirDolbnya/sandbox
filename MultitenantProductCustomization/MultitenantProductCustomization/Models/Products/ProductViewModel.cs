﻿using System.Collections.Generic;
using System.Linq;
using MultiTenantProductCustomization.DomainModel.Products;

namespace MultiTenantProductCustomization.Models.Products
{
	public class ProductViewModel
	{
		public ProductViewModel(Product product) : this()
		{
			Name = product.Name;
			ProductCategoryName = product.ProductCategory.Name;
			Attributes = product
				.Attributes
				.Select(x => new AttributeValueModel {Name = x.Name, AttributeName = x.Attribute.Name, Id = x.Id, AttributeId = x.Attribute.Id})
				.ToList();
		}

		public ProductViewModel()
		{
			Attributes = new List<AttributeValueModel>();
		}

		public string Name { get; set; }

		public string ProductCategoryName { get; set; }

		public List<AttributeValueModel> Attributes { get; set; }
	}
}
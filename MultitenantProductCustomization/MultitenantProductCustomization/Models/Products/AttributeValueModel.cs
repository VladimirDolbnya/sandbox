﻿namespace MultiTenantProductCustomization.Models.Products
{
	public class AttributeValueModel
	{
		public string Name { get; set; }

		public string AttributeName { get; set; }

		public int Id { get; set; }

		public int AttributeId { get; set; }
	}
}
﻿using System.Collections.Generic;
using MultiTenantProductCustomization.Models.Shared;

namespace MultiTenantProductCustomization.Models.Products
{
	public class ProductsListModel
	{
		public int CategoryId { get; set; }

		public string CategoryName { get; set; }

		public List<ItemWitnName> Products { get; set; } 
	}
}
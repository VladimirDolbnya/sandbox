﻿using System.Collections.ObjectModel;
using MultiTenantProductCustomization.DomainModel.Products;

namespace MultiTenantProductCustomization.Models.Products
{
	public class AttributeModel
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public virtual Collection<Product> Products { get; set; }
	}
}
﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using MultiTenantProductCustomization.DomainModel.Products;
using MultiTenantProductCustomization.DomainModel.Products.Configurations;
using MultiTenantProductCustomization.Infastructure.TenantsManagement;

namespace MultiTenantProductCustomization.DomainModel
{
	public class ProductsContext : DbContext
	{
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new ProductCategoryConfiguration());
			modelBuilder.Configurations.Add(new ProductConfiguration());
			modelBuilder.Configurations.Add(new ProductAttributeConfiguration());
			modelBuilder.Configurations.Add(new ProductAttributeValueConfiguration());
			var attributeToTableAnnotationConvention = new AttributeToTableAnnotationConvention<TenantAwareAttribute, string>(TenantAwareAttribute.TenantAnnotation, (type, attributes) => attributes.Single().ColumnName);
			modelBuilder.Conventions.Add(attributeToTableAnnotationConvention);
		}

		public DbSet<Product> Products { get; set; }

		public DbSet<Tenant> Tenants { get; set; }
		
		public DbSet<ProductCategory> ProductCategories { get; set; }

		public DbSet<ProductAttributeValue> ProductAttributeValues { get; set; }

		public DbSet<ProductAttribute> ProductAttributes { get; set; }

		public ProductCategory GetCategory(int categoryId)
		{
			return ProductCategories.Find(categoryId);
		}

		public ProductAttribute GetAtrAttribute(int categoryId)
		{
			return ProductAttributes.Find(categoryId);
		}
	}
}
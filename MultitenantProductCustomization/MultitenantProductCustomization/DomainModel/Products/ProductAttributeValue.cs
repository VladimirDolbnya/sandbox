﻿using System.Collections.ObjectModel;
using MultiTenantProductCustomization.Infastructure.TenantsManagement;

namespace MultiTenantProductCustomization.DomainModel.Products
{
	public class ProductAttributeValue : TenantEntity
	{
		public ProductAttributeValue()
		{
			Products= new Collection<Product>();
		}
		public int Id { get; set; }

		public string Name { get; set; }

		public virtual ProductAttribute Attribute { get; set; }

		public virtual Collection<Product> Products { get; set; }

	}
}
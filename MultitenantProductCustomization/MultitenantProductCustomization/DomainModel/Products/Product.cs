﻿using System.Collections.ObjectModel;
using MultiTenantProductCustomization.Infastructure.TenantsManagement;

namespace MultiTenantProductCustomization.DomainModel.Products
{
	public class Product : TenantEntity
	{
		public Product()
		{
			Attributes = new Collection<ProductAttributeValue>();
		}

		public int Id { get; set; }

		public string Name { get; set; }

		public virtual ProductCategory ProductCategory { get; set; }

		public virtual Collection<ProductAttributeValue> Attributes { get; set; }
	}
}
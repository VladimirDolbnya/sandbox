﻿using System.Collections.ObjectModel;
using MultiTenantProductCustomization.Infastructure.TenantsManagement;

namespace MultiTenantProductCustomization.DomainModel.Products
{
	public class ProductAttribute : TenantEntity
	{
		public ProductAttribute()
		{
			AttributeValues = new Collection<ProductAttributeValue>();
		}

		public int Id { get; set; }

		public string  Name { get; set; }

		public virtual Collection<ProductAttributeValue> AttributeValues { get; set; }
	}
}
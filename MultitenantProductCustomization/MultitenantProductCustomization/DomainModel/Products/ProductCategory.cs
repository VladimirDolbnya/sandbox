﻿using System.Collections.ObjectModel;
using MultiTenantProductCustomization.Infastructure.TenantsManagement;

namespace MultiTenantProductCustomization.DomainModel.Products
{
	public class ProductCategory : TenantEntity
	{
		public ProductCategory()
		{
			Attributes = new Collection<ProductAttribute>();
		}

		public int Id { get; set; }

		public string Name { get; set; }

		public virtual Collection<ProductAttribute> Attributes { get; set; }
	}
}
﻿using System.Data.Entity.ModelConfiguration;

namespace MultiTenantProductCustomization.DomainModel.Products.Configurations
{
	public class ProductConfiguration : EntityTypeConfiguration<Product>
	{
		public ProductConfiguration()
		{
			HasRequired(x => x.Tenant)
				.WithMany()
				.HasForeignKey(pc => pc.TenantId)
				.WillCascadeOnDelete(false);
		}
	}
}
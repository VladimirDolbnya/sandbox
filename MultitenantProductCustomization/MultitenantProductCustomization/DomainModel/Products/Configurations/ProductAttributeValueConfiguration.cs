﻿using System.Data.Entity.ModelConfiguration;

namespace MultiTenantProductCustomization.DomainModel.Products.Configurations
{
	public class ProductAttributeValueConfiguration : EntityTypeConfiguration<ProductAttributeValue>
	{
		public ProductAttributeValueConfiguration()
		{
			HasRequired(x => x.Tenant)
				.WithMany()
				.HasForeignKey(pc => pc.TenantId)
				.WillCascadeOnDelete(false);
		}
	}
}
﻿using System.Data.Entity.ModelConfiguration;

namespace MultiTenantProductCustomization.DomainModel.Products.Configurations
{
	public class ProductAttributeConfiguration : EntityTypeConfiguration<ProductAttribute>
	{
		public ProductAttributeConfiguration()
		{
			HasRequired(x => x.Tenant)
				.WithMany()
				.HasForeignKey(pc => pc.TenantId)
				.WillCascadeOnDelete(false);
		}
	}
}
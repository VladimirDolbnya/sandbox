﻿using System.Data.Entity.ModelConfiguration;

namespace MultiTenantProductCustomization.DomainModel.Products.Configurations
{
	public class ProductCategoryConfiguration : EntityTypeConfiguration<ProductCategory>
	{
		public ProductCategoryConfiguration()
		{
			HasRequired(x => x.Tenant)
				.WithMany()
				.HasForeignKey(pc => pc.TenantId)
				.WillCascadeOnDelete(false);
		}
	}
}